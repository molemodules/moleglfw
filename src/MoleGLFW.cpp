#include "moleGLFW_internal.h"

#include <iostream>

namespace mole::GLFW {

	static MoleGLFW context{};

	void MoleGLFW::KeyPressed(char key) {
		throw;
	}

	void MoleGLFW::CreateContext(unsigned width, unsigned height, std::string name)
	{
		glfwSetErrorCallback(ErrorCallback);

		glfwInit();
		glfwWindowHint(GLFW_SAMPLES, 4);
		glfwWindowHint(GLFW_DEPTH_BITS, 16);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

		window = glfwCreateWindow(width, height, name.c_str(), NULL, NULL);
		if (!window) {
			glfwTerminate();
			ErrorCallback(1, "ERROR failed to create the window");
		}
		else {
			/* FULL SCREEN
			monitor = glfwGetPrimaryMonitor();
			const GLFWvidmode* mode = glfwGetVideoMode(monitor);
			glfwSetWindowMonitor(window, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
			*/

			glfwSetWindowPos(window, 200, 10);

			glfwSetKeyCallback(window, KeyCallback);
			glfwSetMouseButtonCallback(window, MouseButtonCallback);
			glfwSetCursorPosCallback(window, MouseCursorPosCallback);

			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

			glfwMakeContextCurrent(window);
			glfwSwapInterval(1);

			std::cout << "[GLFW context OK]\n";
		}
	}

	void MoleGLFW::FinalizeContext() {

		glfwDestroyWindow(window);
		glfwTerminate();

		std::cout << "[GLFW terminated OK]\n";
	}

	GLFWglproc MoleGLFW::GetProcess(const char* name) {
		return glfwGetProcAddress(name);
	}

	void mole::GLFW::ErrorCallback(int code, const char* message) {
		std::cout << "[MOLEGLFW ERROR " << code << "] \"" << message << "\"\n";
	}

	void mole::GLFW::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
		throw;
	}

	void mole::GLFW::MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
	{
		throw;
	}

	void mole::GLFW::MouseCursorPosCallback(GLFWwindow* window, double x, double y)
	{
		throw;
	}
}