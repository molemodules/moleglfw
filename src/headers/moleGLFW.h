#ifndef MOLEGLFW
#define MOLEGLFW

#include <string>

namespace mole::GLFW {

	struct MoleGLFW;
}

extern "C" {

	// 3rd party required exports  
	struct GLFWwindow;
	struct GLFWmonitor;
	typedef void (*GLFWglproc)(void);
#if defined(_WIN32) && defined(_GLFW_BUILD_DLL)
	/* We are building GLFW as a Win32 DLL */
#define GLFWAPI __declspec(dllexport)
#elif defined(_WIN32) && defined(GLFW_DLL)
	/* We are calling a GLFW Win32 DLL */
#define GLFWAPI __declspec(dllimport)
#elif defined(__GNUC__) && defined(_GLFW_BUILD_DLL)
	/* We are building GLFW as a Unix shared library */
#define GLFWAPI __attribute__((visibility("default")))
#else
#define GLFWAPI
#endif
	GLFWAPI GLFWglproc glfwGetProcAddress(const char* procname);
}

struct mole::GLFW::MoleGLFW
{
	void KeyPressed(char key);
	void CreateContext(unsigned width, unsigned height, std::string name);
	void FinalizeContext();
	GLFWglproc GetProcess(const char* name);
	GLFWwindow* window;
	GLFWmonitor* monitor;
};

#endif //!MOLEGLFW